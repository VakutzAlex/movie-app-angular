import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {AuthService} from "../auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent {
  viewType: string = "login";
  email: string = "";
  password = "";
  passwordRegister = "";
  passwordRegisterConfirm = "";

  username = "";

  constructor(private router: Router, private authService: AuthService) {

  }

  onClearEmail(): void {
    console.log("Before: " + this.email);
    this.email = "";
    console.log("After: " + this.email);
  }

  onRegisterClick(): void {
    this.viewType = "register";
  }

  onLoginClick(): void {
    this.viewType = "login";
  }

  onSubmitLogin(): void {
    this.authService.login(this.email, this.password).subscribe((response: any) => {
      console.log(response);
      this.router.navigate(["/", "dashboard"]);
    });
  }

  onSubmitRegister(): void {
    if (this.passwordRegister != this.passwordRegisterConfirm){
      alert("Passwords do not match");
      return;
    }
    this.authService.register(this.username, this.email, this.passwordRegister, this.passwordRegisterConfirm).subscribe((response: any) => {
      console.log(response);
      this.viewType = "login";
    });
  }
}
