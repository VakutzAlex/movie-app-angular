import { Component } from '@angular/core';

@Component({
  selector: 'app-hello-world',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'MovieManager';
}
